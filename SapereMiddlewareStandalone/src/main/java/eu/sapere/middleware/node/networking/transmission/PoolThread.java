package eu.sapere.middleware.node.networking.transmission;

import java.util.HashMap;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.node.NodeManager;

public final class PoolThread {
	private HashMap<String, ThreadSender> senders;
	private static PoolThread singleton = null;

	/**
	 * @return
	 */
	public static PoolThread getInstance() {
		if (singleton == null)
			singleton = new PoolThread();
		return singleton;
	}

	private PoolThread() {
		senders = new HashMap<String, ThreadSender>();
	}

	/**
	 * @param ip
	 * @param port
	 */
	public synchronized void addNewThread(String ip, int port) {
		if (senders.containsKey(ip))
			return;

		ThreadSender sender = new ThreadSender(this, ip, port);
		// create a new thread sender
		senders.put(ip, sender);
		sender.start();
	}

	/**
	 * @param lsa
	 * @param ipDest
	 */
	public synchronized void pushLsa(Lsa lsa, String ipDest) {
		addNewThread(ipDest, NodeManager.PORT);
		// push an lsa into the queue of the associated thread sender
		senders.get(ipDest).pushLsa(lsa);
	}

	/**
	 * @param s
	 */
	public synchronized void onThreadExit(ThreadSender s) {
		this.senders.remove(s.ipDest);
	}

	/**
	 * @param ipDest
	 */
	public synchronized void removeSenderThread(String ipDest) {
		ThreadSender sender = this.senders.get(ipDest);
		if (sender == null)
			return;
		// stop the thread
		sender.forceThreadStop();
	}
}
