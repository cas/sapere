import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  //readonly baseAppUrl: string = 'http://localhost:9090/';
  readonly baseAppUrl: string = '/app';

  constructor() { }
}
