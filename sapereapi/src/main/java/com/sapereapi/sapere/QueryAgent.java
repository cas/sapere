package com.sapereapi.sapere;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class QueryAgent extends SapereAgent {

	private List<Lsa> results = new ArrayList<Lsa>();
	private String selectedResult;
	private Lsa selectedLsa;
	private String[] prop;
	private String[] waiting;
	private String[] values;
	private Random rand = new Random();
	private boolean alreadyRewarded;

	public QueryAgent(String agentName, String[] subdescription, String[] propertiesName, String[] values,
			LsaType type) {

		super(agentName, subdescription, propertiesName, type);
		selectedLsa = null;
		selectedResult = "";
		this.agentName = agentName;
		this.prop = propertiesName;
		this.waiting = subdescription;
		this.values = values;
		alreadyRewarded = false;

		setInitialLSA();
		query();
	}

	private void reward(Lsa lsaResult, int reward) {
		System.out.println("rewarded-->" + lsaResult.getAgentName() +" by "+ reward);
		System.out.println("lsaResult: "+lsaResult.toVisualString());
		
		if(lsaResult.getAgentName().contains("*")) {
			lsaResult.addSyntheticProperty(SyntheticPropertyName.TYPE, LsaType.Reward);
			sendTo(lsaResult, lsaResult.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
			}
		else
			this.rewardLsa(lsaResult, agentName, reward, 0.0);
	}

	public void query() {
		for (int i = 0; i < prop.length; i++) {
			Property proprety = new Property(prop[i], values[i], agentName, "",
					Arrays.toString(waiting).replace("[", "").replace("]", "") + "|"
							+ Arrays.toString(prop).replace("[", "").replace("]", ""),
					NodeManager.localIP, false);
			lsa.addProperty(proprety);
		}
		lsa.addSyntheticProperty(SyntheticPropertyName.QUERY, lsa.getAgentName());
		lsa.addSyntheticProperty(SyntheticPropertyName.DIFFUSE, "1");
		lsa.addSyntheticProperty(SyntheticPropertyName.DECAY, "4");
		lsa.addSyntheticProperty(SyntheticPropertyName.GRADIENT_HOP, "3");
		lsa.addSyntheticProperty(SyntheticPropertyName.STATE, Arrays.toString(waiting).replaceAll("\\[|\\]", "") + "|"
				+ Arrays.toString(prop).replaceAll("\\[|\\]", ""));
		System.out.println("query injected" + lsa.toVisualString());
	}

	@Override
	public void onBondNotification(BondEvent event) {

		Lsa boundedLsa = event.getBondedLsa();
		System.out.println("Bond event : ** Agent ** " + agentName + " - " + boundedLsa.getAgentName());
		if (!boundedLsa.getPropertiesByQueryAndName(agentName, waiting[0]).isEmpty()) {
			if (!results.contains(boundedLsa)) {
				results.add(boundedLsa);
			}
		}

	}

	@Override
	public void onDecayedNotification(DecayedEvent event) { // change to return only one result
		if (Integer.parseInt(lsa.getSyntheticProperty(SyntheticPropertyName.DECAY).toString()) == 0) {
			if (!results.isEmpty()) {
				Collections.shuffle(results);
				selectedLsa = results.get(0);
				Property propResult = selectedLsa.getPropertiesByQueryAndName(agentName, waiting[0]).get(0);
				selectedResult = propResult.toString();
				propResult.setChosen(true);
				System.out.println("-->result: " + propResult.toString());
			} else {
				System.out.println("no result found");
			}
		}
	}

	public boolean rewardLsaFromApi(int reward) {
		if (selectedLsa != null && !alreadyRewarded) {
			reward(selectedLsa,reward);
			return true;
		}
		return false;
	}

	public String getSelectedResult() {
		return selectedResult;
	}

	public void setSelectedResult(String selectedResult) {
		this.selectedResult = selectedResult;
	}

	public Lsa getSelectedLsa() {
		return selectedLsa;
	}

	public void setSelectedLsa(Lsa selectedLsa) {
		this.selectedLsa = selectedLsa;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {

	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {

	}

	@Override
	public void setInitialLSA() {
		this.submitOperation();
	}

}
