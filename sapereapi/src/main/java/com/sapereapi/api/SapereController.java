package com.sapereapi.api;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.*;
import com.sapereapi.model.Sapere;
import com.sapereapi.model.Service;

import eu.sapere.middleware.lsa.Lsa;

@RestController
public class SapereController {

	@GetMapping(value = "/startSapere")
	public String startsapere() {
		System.out.println("SAPERE starting...");
		return Sapere.getInstance().getInfo();
	}

	@GetMapping(value = "/diffuse")
	public String diffuseLsa(@RequestParam String name, @RequestParam int hops) {
		Sapere.getInstance().diffuseLsa(name, hops);
		return Sapere.getInstance().getLsa(name);
	}
	
	@GetMapping(value = "/info")
	public String getInfo() {
		return Sapere.getInstance().getInfo();
	}
	
	@GetMapping(value = "/lsasList")
	public List<Service> getLsaList() {
		return Sapere.getInstance().getServices();
	}
	
	@GetMapping(value = "/node")
	public List<String> getNodes() {
		return Sapere.getInstance().getNodes();
	}
	
	@GetMapping(value = "/lsas")
	public List<String> getLsas() {
		return Sapere.getInstance().getLsa();
	}

    @GetMapping(value="/qtable/{name}")
    public Map<String, Double[]> getQtable(@PathVariable String name) {
        return Sapere.getInstance().getQtable(name);
    }

    @GetMapping(value="/lsa/{name}")
    public String getLsa(@PathVariable String name) {
        return Sapere.getInstance().getLsa(name);
    }

}
