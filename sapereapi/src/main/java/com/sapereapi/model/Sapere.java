package com.sapereapi.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.sapereapi.sapere.AgentBloodSearch;
import com.sapereapi.sapere.AgentTransport;
import com.sapereapi.sapere.QueryAgent;
import com.sapereapi.sapere.ServiceAgent;
import com.sapereapi.sapere.ServiceAgentWeb;

import eu.sapere.middleware.agent.Agent;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.LsaType;
import eu.sapere.middleware.node.NodeManager;

public class Sapere {
	
	private List<QueryAgent> querys;
	public static List<SapereAgent> ServiceAgents;
	private static Sapere instance = null;
	public NodeManager nodeManager;

	public static Sapere getInstance() {
		if (instance == null) {
			instance = new Sapere();
		}
		return instance;
	}

	public Sapere() {
		nodeManager = nodeManager.instance();
		nodeManager.getNetworkDeliveryManager().setNeighbours(null);
		nodeManager.localIP = "";
		nodeManager.nodeName = "";	
		
		querys = new ArrayList<QueryAgent>();
		ServiceAgents = new ArrayList<SapereAgent>();
	}

	public String getInfo() {
		return NodeManager.nodeName +" - "+ NodeManager.localIP+" -: "+ Arrays.toString(NodeManager.instance().networkDeliveryManager.getNeighbours());
	}
	
	public void diffuseLsa(String lsaName, int hops) {
		for (SapereAgent service : ServiceAgents) {
			if (lsaName.equals(service.getAgentName())) {
				service.addGradient(hops);
				break;
			}
		}
	}

	public List<String> getLsa() {
		List<String> lsaList = new ArrayList<String>();

		for (Lsa lsa: NodeManager.instance().getSpace().getAllLsa().values()) {
			lsaList.add(lsa.toVisualString());
		}
		return lsaList;
	}

	public List<Service> getLsas() {
		List<Service> serviceList = new ArrayList<Service>();
		for (SapereAgent service : ServiceAgents) {
			serviceList.add(new Service(service));
		}
		return serviceList;
	}

	public Map<String, Double[]> getQtable(String name) {
		for (SapereAgent serviceAgent : ServiceAgents) {
			if (serviceAgent.getAgentName().equals(name)) {
				return serviceAgent.getQ();
			}
		}
		return null;
	}

	public String getLsa(String name) {
		String visualLsa = "";
		for (Lsa lsa : NodeManager.instance().getSpace().getAllLsa().values()) {
			if (name.equals(lsa.getAgentName())) {
				visualLsa = lsa.toVisualString();
				break;
			}
		}
		return visualLsa;
	}

	// Add and start a service
	public void addServiceGeneric(Service service) {
		ServiceAgents
				.add(new ServiceAgent(service.getName(), service.getInput(), service.getOutput(),service.getUrl(), LsaType.Service));
		startService(service.getName());
	}
	public void addServiceRest(Service service) {
		ServiceAgents
				.add(new ServiceAgentWeb(service.getUrl(), service.getName(), service.getInput(), service.getOutput(),service.getAppid(), LsaType.Service));
		startService(service.getName());
	}

	public void addServiceBlood(Service service) {
		ServiceAgents.add(new AgentBloodSearch(service.getName(), new String[] { "Blood" },
				new String[] { "Position" }, LsaType.Service));
		startService(service.getName());
	}

	public void addServiceBlood(String name, String type) {
		ServiceAgents.add(
				new AgentBloodSearch(name, new String[] { "Blood" }, new String[] { "Position" }, LsaType.Service));
		startService(name);
	}

	public void addServiceTransport(Service service) {
		ServiceAgents.add(new AgentTransport(service.getName(), new String[] { "Position", "Destination" },
				new String[] { "Transport" }, LsaType.Service));
		startService(service.getName());
	}

	public void addServiceTransport(String name, String type) {
		ServiceAgents.add(new AgentTransport(name, new String[] { "Position", "Destination" },
				new String[] { "Transport" }, LsaType.Service));
		startService(name);
	}

	public List<Service> getServices() {
		List<Service> services = new ArrayList<Service>();
		for (SapereAgent service : ServiceAgents) {
			services.add(new Service(service));
		}
		return services;
	}
	
	public List<String> getNodes() {
		HashSet<String> nodeSet= new HashSet<>();
		for (SapereAgent service : ServiceAgents) {
			nodeSet.add(service.getInput()[0]);
			nodeSet.add(service.getOutput()[0]);
		}
		List<String> nodes = new ArrayList<String>(nodeSet);
		return nodes;
	}

	public static void startService(String name) {
		for (SapereAgent serviceAgent : ServiceAgents) {
			if (serviceAgent.getAgentName().equals(name)) {
				serviceAgent.setInitialLSA();
			}
		}
	}

	public void addQuery(Query request) {
		querys.add(new QueryAgent(request.getName(), request.getWaiting(), request.getProp(), request.getValues(),
				LsaType.Query));
	}

	public QueryAgent getQueryByName(String name) {
		for (QueryAgent query : querys) {
			if (query.getAgentName().equals(name)) {
				return query;
			}
		}
		return null;
	}

	public List<QueryAgent> getQuerys() {
		return querys;
	}

	public void setQuerys(List<QueryAgent> querys) {
		this.querys = querys;
	}

}
